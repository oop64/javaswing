/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author user1
 */
public class JPopupMenuExample {

    public static void main(String[] args) {
        final JFrame f = new JFrame("PopupMenu Example");
        
        final JPopupMenu popupmenu = new JPopupMenu("Edit");
        
        JMenuItem cut = new JMenuItem("Cut");
        
        JMenuItem copy = new JMenuItem("Copy");
        
        JMenuItem paste = new JMenuItem("Paste");
        
        popupmenu.add(cut);
        popupmenu.add(copy);
        popupmenu.add(paste);
        
        f.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                popupmenu.show(f, e.getX(), e.getY());
            }
        });

        f.add(popupmenu);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
