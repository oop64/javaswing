/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author user1
 */
public class JRadioButtonExample {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        
        JLabel l1 = new JLabel("Choose the correct answer"); 
        l1.setBounds(5, 0, 200, 30);
        
        JLabel l2 = new JLabel("1.Which subject is the hardest"); 
        l2.setBounds(20, 20, 200, 30);

        JRadioButton r1 = new JRadioButton("A) OOP");
        r1.setBounds(50, 50, 150, 30);
        
        JRadioButton r2 = new JRadioButton("B) DataBase");
        r2.setBounds(50, 80, 150, 30);
        
        JRadioButton r3 = new JRadioButton("C) Stat");
        r3.setBounds(50, 110, 150, 30);
        
        JRadioButton r4 = new JRadioButton("D) All subject");
        r4.setBounds(50, 140, 150, 30);
        
        ButtonGroup bg = new ButtonGroup();
        
        JButton b = new JButton("Send");
        b.setBounds(170, 170, 80, 40);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(r4.isSelected()){
                    JOptionPane.showMessageDialog(r4, "Correct");
                }else{
                    JOptionPane.showMessageDialog(r1, "Correct answer: D)");
                }
            }
        });
        
        bg.add(r1);
        bg.add(r2);
        bg.add(r3);
        bg.add(r4);
        f.add(b);
        f.add(l1);
        f.add(l2);
        f.add(r1);
        f.add(r2);
        f.add(r3);
        f.add(r4);
        f.setSize(300, 250);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
