/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author user1
 */
public class JTextFieldExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("TextField Example");
        JTextField t1 = new JTextField("Hi,");
        t1.setEditable(false);   
        t1.setBounds(50, 50, 200, 30);
        
        JTextField t2 = new JTextField("(Edit your name here)");
        t2.setBounds(50, 100, 200, 30);
        
        JButton b = new JButton("Enter");
        b.setBounds(250, 100, 100, 30);
        
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = t2.getText();
                    t1.setText("Hi, " + name);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        });

        f.add(t1);
        f.add(t2);
        f.add(b);
        f.setSize(400, 200);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    

    }

}
