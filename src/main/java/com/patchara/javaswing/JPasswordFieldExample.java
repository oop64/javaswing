/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author user1
 */
public class JPasswordFieldExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Password Field Example");

        JLabel l1 = new JLabel("Username :");
        l1.setBounds(30, 10, 70, 30);

        JTextField tf = new JTextField();
        tf.setBounds(100, 10, 100, 30);

        final JPasswordField value = new JPasswordField();
        value.setBounds(100, 50, 100, 30);

        JLabel l2 = new JLabel("Password :");
        l2.setBounds(30, 50, 70, 30);

        JButton b1 = new JButton("Login");
        b1.setBounds(200, 50, 100, 30);

        final JLabel l3 = new JLabel();
        l3.setBounds(80, 80, 200, 30);

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = tf.getText();
                l3.setText("Welcome " + name);
            }
        });

        final JLabel l4 = new JLabel();
        l4.setBounds(200, 110, 70, 30);

        JButton b2 = new JButton("ShowPassword");
        b2.setBounds(50, 110, 120, 30);
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String pass = new String(value.getPassword());
                l4.setText(pass);
            }
        });

        f.add(l1);
        f.add(tf);
        f.add(l2);
        f.add(l3);
        f.add(l4);
        f.add(value);
        f.add(b1);
        f.add(b2);
        f.setSize(300, 200);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
