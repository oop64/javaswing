/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author user1
 */
public class JOptionPaneExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("JOptionPane Example");
        
        JLabel l1 = new JLabel("Input score :");
        l1.setBounds(10, 10, 80, 30);
        
        JTextField tf = new JTextField();
        tf.setBounds(90, 10, 80, 30);
        
        JButton b = new JButton("Enter");
        b.setBounds(170, 10, 60, 30);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int deci = JOptionPane.showConfirmDialog(f,"After score saved you cannot change. Are you sure?");
                if(deci==JOptionPane.YES_OPTION){
                    String name=JOptionPane.showInputDialog(f,"Student ID :"); 
                    JOptionPane.showMessageDialog(f,"Score Saved.","Alert",JOptionPane.CLOSED_OPTION);
                }else{
                    tf.requestFocus();
                }
            }
        });
        
        f.add(b);
        f.add(tf);
        f.add(l1);
        f.setSize(250, 100);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }

}
