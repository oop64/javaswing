/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;

/**
 *
 * @author user1
 */
public class JScrollBarExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Scrollbar Example");
        
        JScrollBar s = new JScrollBar();
        s.setBounds(100, 100, 50, 100);
        
        f.add(s);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

}
