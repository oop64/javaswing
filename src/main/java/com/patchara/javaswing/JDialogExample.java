/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author user1
 */
public class JDialogExample {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        JDialog d = new JDialog(f, "Dialog Example", true);
        d.setLayout(new FlowLayout());
        JButton b = new JButton("OK");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                d.setVisible(false);
            }
        });
        d.add(new JLabel("Click button to continue."));
        d.add(b);
        d.setSize(200, 100);
        d.setVisible(true);
    }
}
