/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;

/**
 *
 * @author user1
 */
public class JTabbedPaneExample {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        
        JTextArea ta = new JTextArea(200, 200);
        
        JPanel p1 = new JPanel();
        p1.add(ta);
        
        JPanel p2 = new JPanel();
        
        JPanel p3 = new JPanel();
        JLabel lb = new JLabel("GooGle can help you:)");
        lb.setBounds(150, 300, 100, 30);
        p3.add(lb);
        
        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(50, 30, 300, 300);
        tp.add("main", p1);
        tp.add("visit", p2);
        tp.add("help", p3);
        
        f.add(tp);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }
}
