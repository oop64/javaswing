/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import java.awt.event.*;  
import javax.swing.*;

/**
 *
 * @author user1
 */
public class JCheckBoxExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("CheckBox Example");
        
        JLabel l1 = new JLabel("Homework list:");
        l1.setBounds(10, 10, 100, 50);
        
        JCheckBox cb1 = new JCheckBox("Calculus");
        cb1.setBounds(50, 40, 100, 50);
        
        JCheckBox cb2 = new JCheckBox("OOP");
        cb2.setBounds(50, 70, 100, 50);
        
        JCheckBox cb3 = new JCheckBox("DataBase");
        cb3.setBounds(50, 100, 100, 50);
        
        JButton b = new JButton("Check");
        b.setBounds(100, 150, 80, 40);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(cb1.isSelected()&&cb2.isSelected()&&cb3.isSelected()){
                    JOptionPane.showMessageDialog(cb1, "All Finished!!");
                }else{
                    JOptionPane.showMessageDialog(cb1, "Recheck your homework");
                }
            }
        });
        
        f.add(l1);
        f.add(cb1);
        f.add(cb2);
        f.add(cb3);
        f.add(b);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
    }
}
