/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author user1
 */
public class JListExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("JList Example");

        JLabel l1 = new JLabel("Clolor Type : ");
        l1.setBounds(10, 10, 100, 30);

        DefaultListModel<String> l2 = new DefaultListModel<>();
        l2.addElement("Water Color");
        l2.addElement("Poster Color");
        l2.addElement("Oil Color");
        l2.addElement("Acrylic Color");
        l2.addElement("Tempere");
        l2.addElement("Grayon Color");
        l2.addElement("Chalk Color");

        final JList<String> list = new JList<>(l2);
        list.setBounds(100, 20, 130, 120);

        JLabel lb = new JLabel("Color type : ");
        lb.setBounds(140, 150, 200, 30);

        JButton b = new JButton("Enter");
        b.setBounds(50, 150, 80, 30);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (list.getSelectedValue() != null) {
                    String color = list.getSelectedValue();
                    lb.setText("Color type : " + color);
                }else{
                    lb.setText("Color type : Choose Answer");
                }
            }
        });

        f.add(l1);
        f.add(lb);
        f.add(b);
        f.add(list);
        f.setSize(350, 250);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
