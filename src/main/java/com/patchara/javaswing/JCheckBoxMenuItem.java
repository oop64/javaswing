/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import java.awt.event.ActionEvent;  
import java.awt.event.ActionListener;  
import java.awt.event.KeyEvent;  
import javax.swing.AbstractButton;  
import javax.swing.Icon;  
import javax.swing.JCheckBoxMenuItem;  
import javax.swing.JFrame;  
import javax.swing.JMenu;  
import javax.swing.JMenuBar;  
import javax.swing.JMenuItem;  

/**
 *
 * @author user1
 */

  
public class JCheckBoxMenuItem {  
    public static void main(final String args[]) {  
        JFrame frame = new JFrame("Jmenu Example");  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JMenuBar menuBar = new JMenuBar();  

        JMenu fileMenu = new JMenu("File");  
        fileMenu.setMnemonic(KeyEvent.VK_F);  
        menuBar.add(fileMenu);  

        JMenuItem menuItem1 = new JMenuItem("Close", KeyEvent.VK_N);  
        fileMenu.add(menuItem1);  
  
        JCheckBoxMenuItem caseMenuItem = new JCheckBoxMenuItem("Open");  
        caseMenuItem.setMnemonic(KeyEvent.VK_C);  
        fileMenu.add(caseMenuItem);  
  
        ActionListener aListener = new ActionListener() {  
            @Override
            public void actionPerformed(ActionEvent event) {  
                AbstractButton aButton = (AbstractButton) event.getSource();  
                boolean selected = aButton.getModel().isSelected();  
                String newLabel;  
                Icon newIcon;  
                if (selected) {  
                    newLabel = "Save as";  
                } else {  
                    newLabel = "Save";  
                }  
                aButton.setText(newLabel);  
            }  
        };  
  
        caseMenuItem.addActionListener(aListener);  
        frame.setJMenuBar(menuBar);  
        frame.setSize(350, 250);  
        frame.setVisible(true);  
    }  
}  