/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import java.awt.*;
import javax.swing.JFrame;

/**
 *
 * @author user1
 */
public class DisplayingImage extends Canvas {

    @Override
    public void paint(Graphics g) {
        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("Desktop:Bo.png");
        g.drawImage(i, 120, 100, this);

    }

    public static void main(String[] args) {
        DisplayingImage m = new DisplayingImage();
        JFrame f = new JFrame();
        f.add(m);
        f.setSize(400, 400);
        f.setVisible(true);
    }
}
