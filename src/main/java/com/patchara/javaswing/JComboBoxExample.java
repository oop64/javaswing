/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author user1
 */
public class JComboBoxExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("ComboBox Example");

        JLabel l1 = new JLabel("Choose color type you like.");
        l1.setBounds(20, 20, 200, 30);

        String type[] = {"Water Color", "Poster Color", "Oil Color", "Acrylic Color", "Tempere", "Grayon Color", "Chalk Color"};

        JComboBox cb = new JComboBox(type);
        cb.setBounds(50, 55, 120, 30);

        JLabel l2 = new JLabel("color type you choose : ");
        l2.setBounds(20, 90, 300, 30);

        JButton b = new JButton("Select");
        b.setBounds(190, 55, 100, 30);
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String data = "color type you choose : "
                        + cb.getItemAt(cb.getSelectedIndex());
                l2.setText(data);
            }
        });

        f.add(cb);
        f.add(l1);
        f.add(l2);
        f.add(b);
        f.setLayout(null);
        f.setSize(350, 150);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
