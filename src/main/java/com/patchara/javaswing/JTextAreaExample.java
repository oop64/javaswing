/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author user1
 */
public class JTextAreaExample {

    public static void main(String[] args) {
        JFrame f = new JFrame();

        JLabel l = new JLabel("Happy Birth Day :");
        l.setBounds(10, 10, 200, 30);
        
        JLabel l1 = new JLabel("Words : ");
        l1.setBounds(50, 250, 100, 30);
        JLabel l2 = new JLabel("Characters : ");
        l2.setBounds(150, 250, 100, 30);

        JTextArea area = new JTextArea("(Type your wish here)");
        area.setBounds(50, 40, 200, 200);

        JButton b = new JButton("Enter");
        b.setBounds(250, 210, 100, 30);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = area.getText();
                String words[] = text.split("\\s");
                l1.setText("Words : " + words.length);
                l2.setText("Characters : " + text.length());
            }
        });

        f.add(l);
        f.add(l1);
        f.add(l2);
        f.add(b);
        f.add(area);
        f.setSize(350, 350);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
