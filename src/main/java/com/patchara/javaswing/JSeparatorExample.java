/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author user1
 */
public class JSeparatorExample {

    JMenu menu, submenu;
    JMenuItem i1, i2, i3, i4, i5;

    JSeparatorExample() {
        JFrame f = new JFrame("Separator Example");
        
        JMenuBar mb = new JMenuBar();
        
        menu = new JMenu("Menu");
        i1 = new JMenuItem("Open File");
        i2 = new JMenuItem("Quit Program");
        
        menu.add(i1);
        menu.addSeparator();
        menu.add(i2);
        mb.add(menu);
        
        f.setJMenuBar(mb);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        JFrame f = new JFrame("Separator Example");
        new JSeparatorExample();
    }
}
