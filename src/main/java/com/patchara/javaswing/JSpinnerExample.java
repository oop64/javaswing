/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;

/**
 *
 * @author user1
 */
public class JSpinnerExample {

    public static void main(String[] args) {
        JFrame f = new JFrame("Spinner Example");

        SpinnerModel value = new SpinnerNumberModel(1, 0, 10, 1);

        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(50, 50, 50, 30);

        f.add(spinner);
        f.setSize(150, 150);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
