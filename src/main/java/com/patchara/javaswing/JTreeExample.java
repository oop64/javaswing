/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author user1
 */
public class JTreeExample {

    public static void main(String[] args) {
        
        JFrame f = new JFrame();
        
        DefaultMutableTreeNode style = new DefaultMutableTreeNode("Style");
        DefaultMutableTreeNode color = new DefaultMutableTreeNode("color");
        DefaultMutableTreeNode font = new DefaultMutableTreeNode("font");
        
        style.add(color);
        style.add(font);
        
        DefaultMutableTreeNode red = new DefaultMutableTreeNode("red");
        DefaultMutableTreeNode blue = new DefaultMutableTreeNode("blue");
        DefaultMutableTreeNode black = new DefaultMutableTreeNode("black");
        DefaultMutableTreeNode green = new DefaultMutableTreeNode("green");
        
        color.add(red);
        color.add(blue);
        color.add(black);
        color.add(green);
        
        DefaultMutableTreeNode ang = new DefaultMutableTreeNode("Angsana New");
        DefaultMutableTreeNode sara = new DefaultMutableTreeNode("TH Sarabun");
        DefaultMutableTreeNode cor = new DefaultMutableTreeNode("Cordia New");
        DefaultMutableTreeNode ari = new DefaultMutableTreeNode("Arial");
        
        font.add(ang);
        font.add(sara);
        font.add(cor);
        font.add(ari);
        
        JTree jt = new JTree(style);
        
        f.add(jt);
        f.setSize(200, 250);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
    }
}
