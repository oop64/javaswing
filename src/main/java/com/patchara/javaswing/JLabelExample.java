/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author user1
 */
public class JLabelExample {

    public static void main(String args[]) {
        JFrame f = new JFrame("Label Example");

        JTextField tf = new JTextField();
        tf.setBounds(50, 50, 150, 20);

        JLabel l1 = new JLabel();
        l1.setBounds(50, 100, 250, 20);

        JButton b = new JButton("Find IP");
        b.setBounds(50, 150, 95, 30);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String host = tf.getText();
                    String ip = java.net.InetAddress.getByName(host).getHostAddress();
                    l1.setText("IP of " + host + " is: " + ip);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        });

        f.add(tf);
        f.add(l1);
        f.add(b);
        f.setSize(500, 500);
        f.setLayout(null);
        f.setVisible(true);
    }

}
