/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.javaswing;
import java.awt.Color;  
import java.awt.Graphics;  
import javax.swing.JComponent;  
import javax.swing.JFrame; 
/**
 *
 * @author user1
 */
public class JComponent {
     public static void main(String[] arguments) {  
        MyJComponent com = new MyJComponent();  

        JFrame.setDefaultLookAndFeelDecorated(true);  
        JFrame frame = new JFrame("JComponent Example");  
        frame.setSize(300,200);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  

        frame.add(com);  
        frame.setVisible(true);  
      }  
}
class MyJComponent extends JComponent {  
      @Override
      public void paint(Graphics g) {  
        g.setColor(Color.green);  
        g.fillRect(30, 30, 100, 100);  
      }  
}  
